import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



class Six extends StatelessWidget {
  const Six({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: const Text('เสนอความคิด'),
      ),
      body: new Center(

        child: ListView(
          children: [
            Text('\n'),
          Column(

            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text('ความคิดเห็น', textAlign: TextAlign.left,style: TextStyle(fontSize: 25.0,fontWeight: FontWeight.w600),),
              TextField(

                cursorColor: Colors.grey,
                decoration: InputDecoration(hintText: '....ความคิดเห็น...',
                  border: OutlineInputBorder(),
                  contentPadding: EdgeInsets.symmetric(vertical: 20),
                ),
                maxLines: 5, // <-- SEE HERE
                minLines: 5,
              ),
              Text('\n'),
              Text('บุคคลที่ต้องการถามคำถาม', textAlign: TextAlign.left,style: TextStyle(fontSize: 25.0,fontWeight: FontWeight.w600),),
              Text('\n'),
              TextField(

                cursorColor: Colors.grey,
                decoration: InputDecoration(hintText: '.......',
                  border: OutlineInputBorder(),
                  contentPadding: EdgeInsets.symmetric(vertical: 20),
                ),
                maxLines: 5, // <-- SEE HERE
                minLines: 5,
              ),

            ],
          ),
            Text('\n'),
            Divider(
              height: 10,
            ),
            Text('\n'),
            TextButton(onPressed: () {}, child: Container(
              color: Colors.grey,
              padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 10),
              child: const Text('ส่งข้อความ',style: TextStyle(color: Colors.white, fontSize: 15.0),
            ),),
            ),

          ],
        ),
      ),
    );
  }
}
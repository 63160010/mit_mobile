import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class One extends StatelessWidget {
  const One({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: const Text('ลงทะเบียน'),
      ),
      body: new Center(

        child: ListView(
          children: [
            Container(
              child: Image(image: NetworkImage('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS677JiDZqiJD9wqTzItjZdD6GpA10suIId_tvBkZTadIRywud0EyqNgnLKufQ-vBvtDrQ&usqp=CAU')
                ,fit: BoxFit.cover,),
            ),
            Text('\n'),
            Divider(
              height: 10,
            ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text('รหัสวิชา', textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.w600),),
              Divider(
                height: 10,
              ),
              TextField(
                cursorColor: Colors.white,
                decoration: InputDecoration(hintText: 'Enter here'),
              )
            ],
          ),
            Text('\n'),
            Divider(
              height: 10,
            ),
            Text('\n'),
            tabel1()


          ],
        ),
      ),
    );
  }
}

class tabel1 extends StatelessWidget {
  const tabel1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Center(
        child: Table(
            border: TableBorder.all(
              color: Colors.yellow,
            ),
            // Allows to add a border decoration around your table
            children: [
              TableRow(children: [
                Container(
                  color: Colors.green,
                  child: Text(
                    'วิชา',
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  color: Colors.green,
                  child: Text(
                    'อาจารย์ผู้สอน',
                    textAlign: TextAlign.center,
                  ),),
                Container(
                  color: Colors.green,
                  child: Text(
                    'กลุ่ม',
                    textAlign: TextAlign.center,
                  ),),
          Container(
            color: Colors.green,
            child: Text(
              'ที่นั่ง',
              textAlign: TextAlign.center,
            ),),
          Container(
            color: Colors.green,
            child: Text(
              'เหลือ',
              textAlign: TextAlign.center,
            ),),
              ]),
            ]),
      ),
    );
  }
}
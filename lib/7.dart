import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Seven extends StatelessWidget {
  const Seven({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: const Text('ประวัติการเข้าใช้ระบบ'),
      ),
      body: new Center(

        child: ListView(
          children: [
            Container(
              child: Image(image: NetworkImage('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS677JiDZqiJD9wqTzItjZdD6GpA10suIId_tvBkZTadIRywud0EyqNgnLKufQ-vBvtDrQ&usqp=CAU')
                ,fit: BoxFit.cover,),
            ),
            Text('\n'),
            Divider(
              height: 10,
            ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text('สถิติการเข้าใช้', textAlign: TextAlign.center,style: TextStyle(color: Colors.green, fontSize: 14.0,fontWeight: FontWeight.w600),),
            ],
          ),
            Divider(
              height: 10,
            ),
            Text('\n'),
            tabel1(),
            tabel2()


          ],
        ),
      ),
    );
  }
}

class tabel1 extends StatelessWidget {
  const tabel1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Center(
        child: Table(
            border: TableBorder.all(
              color: Colors.yellow,
            ),
            // Allows to add a border decoration around your table
            children: [
              TableRow(children: [
                Container(
                  color: Colors.green,
                  child: Text(
                    'ครั้งที่',
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  color: Colors.green,
                  child: Text(
                    'เวลา',
                    textAlign: TextAlign.center,
                  ),),
                Container(
                  color: Colors.green,
                  child: Text(
                    'จาก (IP)',
                    textAlign: TextAlign.center,
                  ),),
              ],),

              TableRow(children: [
                Text(
                  '1',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '14:28',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '10.30.51.54',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Text(
                  '2',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '19:47',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '10.30.51.54',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Text(
                  '3',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '9:08',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '10.30.51.54',
                  textAlign: TextAlign.center,
                ),
              ]),

            ]),
      ),
    );
  }
}


class tabel2 extends StatelessWidget {
  const tabel2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Center(
        child: Table(
            border: TableBorder.all(
              color: Colors.yellow,
            ),
            // Allows to add a border decoration around your table
            children: [

            ]),
      ),
    );
  }
}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Two extends StatelessWidget {
  const Two({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: const Text('ผลการลงทะเบียน'),
      ),
      body: ListView(
        children: [
          Text('\n'),
          Text('รายวิชาที่ลงทะเบียนทั้งหมด',textAlign: TextAlign.center,style: TextStyle(color: Colors.green,fontWeight: FontWeight.w600,
            fontSize: 20.0,)),
          Text('\n'),
          tabel1(),
          Text('\n'),
          Text('จำนวนหน่วยกิตรวม_18',textAlign: TextAlign.center,style: TextStyle(color: Colors.green,fontWeight: FontWeight.w600,
            fontSize: 20.0,)),
        ],
      ),
    );
  }
}

class tabel1 extends StatelessWidget {
  const tabel1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Center(
        child: Table(
            border: TableBorder.all(color: Colors.green,), // Allows to add a border decoration around your table
            children: [

              TableRow(children :[
        Container(
        color: Colors.green,
            height: 50,
            child:
                Text('รหัสวิชา',textAlign: TextAlign.center,),),
        Container(
          color: Colors.green,
          height: 50,
          child:
                Text('ชื่อรายวิชา',textAlign: TextAlign.center,),),
        Container(
          color: Colors.green,
          height: 50,
          child:
                Text('แบบการศึกษา',textAlign: TextAlign.center,),),
        Container(
          color: Colors.green,
          height: 50,
          child:
                Text('หน่วยกิต',textAlign: TextAlign.center,),),
        Container(
          color: Colors.green,
          height: 50,
          child:
                Text('กลุ่ม',textAlign: TextAlign.center,),),
                Container(
                  color: Colors.green,
                  height: 50,
                  child:
                Text('เกรด',textAlign: TextAlign.center,),),
              ]),

              TableRow(children :[
                Text('88624359',textAlign: TextAlign.center,),
                Text('Web Programming',textAlign: TextAlign.center,),
                Text('GD',textAlign: TextAlign.center,),
                Text('3',textAlign: TextAlign.center,),
                Text('2',textAlign: TextAlign.center,),
                Text('',textAlign: TextAlign.center,),
              ]),
              TableRow(children :[
                Text('88624459',textAlign: TextAlign.center,),
                Text('Object-Oriented Analysis and Design',textAlign: TextAlign.center,),
                Text('GD',textAlign: TextAlign.center,),
                Text('3',textAlign: TextAlign.center,),
                Text('2',textAlign: TextAlign.center,),
                Text('',textAlign: TextAlign.center,),
              ]),
              TableRow(children :[
                Text('88624559',textAlign: TextAlign.center,),
                Text('Software Testing',textAlign: TextAlign.center,),
                Text('GD',textAlign: TextAlign.center,),
                Text('3',textAlign: TextAlign.center,),
                Text('2',textAlign: TextAlign.center,),
                Text('',textAlign: TextAlign.center,),
              ]),
              TableRow(children :[
                Text('88634259',textAlign: TextAlign.center,),
                Text('Multimedia Programming for Multiplatforms',textAlign: TextAlign.center,),
                Text('GD',textAlign: TextAlign.center,),
                Text('3',textAlign: TextAlign.center,),
                Text('2',textAlign: TextAlign.center,),
                Text('',textAlign: TextAlign.center,),
              ]),
              TableRow(children :[
                Text('88634459',textAlign: TextAlign.center,),
                Text('Mobile Application Development I',textAlign: TextAlign.center,),
                Text('GD',textAlign: TextAlign.center,),
                Text('3',textAlign: TextAlign.center,),
                Text('2',textAlign: TextAlign.center,),
                Text('',textAlign: TextAlign.center,),
              ]),
              TableRow(children :[
                Text('88646259',textAlign: TextAlign.center,),
                Text('Introduction to Natural Language Processing',textAlign: TextAlign.center,),
                Text('GD',textAlign: TextAlign.center,),
                Text('3',textAlign: TextAlign.center,),
                Text('2',textAlign: TextAlign.center,),
                Text('',textAlign: TextAlign.center,),
              ]),
            ]
        ),


      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Three extends StatelessWidget {
  const Three({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
  return new Scaffold(
    appBar: new AppBar(
      title: const Text('ตารางเรียน / สอบ'),
    ),
    body: new Center(

      child: ListView(
        children: [
          Text('ตารางเรียน',textAlign: TextAlign.center,style: TextStyle(color: Colors.blue,fontWeight: FontWeight.w600,
            fontSize: 20.0,)),

          Text('\n'),
          Text('Monday:',style: TextStyle(fontWeight: FontWeight.w600)),
          Text('\n'),
          tabel1(),

          Text('\n'),
          Text('Tuesday:',style: TextStyle(fontWeight: FontWeight.w600)),
          Text('\n'),
          tabel2(),

          Text('\n'),
          Text('Wednesday:',style: TextStyle(fontWeight: FontWeight.w600)),
          Text('\n'),
          tabel3(),

          Text('\n'),
          Text('Friday:',style: TextStyle(fontWeight: FontWeight.w600)),
          Text('\n'),
          tabel4(),


          Text('\n'),
          Text('ตารางสอบ',textAlign: TextAlign.center,style: TextStyle(color: Colors.red,fontWeight: FontWeight.w600,
            fontSize: 20.0,)),
          Text('\n'),
          tabel5(),

        ],
      ),
    ),
  );
  }
}



class tabel1 extends StatelessWidget {
  const tabel1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Center(
        child: Table(
            border: TableBorder.all(
              color: Colors.yellow,
            ),
            // Allows to add a border decoration around your table
            children: [
              TableRow(children: [
                Container(
                  color: Colors.orangeAccent,
                  child: Text(
                    'วิชา:',
                    textAlign: TextAlign.center,
                  ),
                ),
        Container(
            color: Colors.orangeAccent,
            child: Text(
                  'ห้องเรียน',
                  textAlign: TextAlign.center,
                ),),
          Container(
              color: Colors.orangeAccent,
              child: Text(
                  'เวลา',
                  textAlign: TextAlign.center,
                ),),
              ]),


              TableRow(children: [
                Text(
                    '88624559-59',
                    textAlign: TextAlign.center,
                  ),
                Text(
                  'IF-4M210',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '10:00-12:00',
                  textAlign: TextAlign.center,
                ),
              ]),

              TableRow(children: [
                Text(
                  '88624459-59',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'IF-3M210',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '13:00-15:00',
                  textAlign: TextAlign.center,
                ),
              ]),

              TableRow(children: [
                Text(
                  '88624359-59',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'IF-3M210',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '17:00-19:00',
                  textAlign: TextAlign.center,
                ),
              ]),
            ]),
      ),
    );
  }
}


class tabel2 extends StatelessWidget {
  const tabel2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Center(
        child: Table(
            border: TableBorder.all(
              color: Colors.pink,
            ),
            // Allows to add a border decoration around your table
            children: [
              TableRow(children: [
                Container(
                  color: Colors.pinkAccent,
                  child: Text(
                    'วิชา:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  color: Colors.pinkAccent,
                  child: Text(
                    'ห้องเรียน',
                    textAlign: TextAlign.center,
                  ),),
                Container(
                  color: Colors.pinkAccent,
                  child: Text(
                    'เวลา',
                    textAlign: TextAlign.center,
                  ),),
              ]),


              TableRow(children: [
                Text(
                  '88634259-59',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'IF-4C02',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '10:00-12:00',
                  textAlign: TextAlign.center,
                ),
              ]),

              TableRow(children: [
                Text(
                  '88624559-59',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'IF-3C03',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '13:00-15:00',
                  textAlign: TextAlign.center,
                ),
              ]),

              TableRow(children: [
                Text(
                  '88624459-59',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'IF-3C01',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '17:00-19:00',
                  textAlign: TextAlign.center,
                ),
              ]),
            ]),
      ),
    );
  }
}



class tabel3 extends StatelessWidget {
  const tabel3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Center(
        child: Table(
            border: TableBorder.all(
              color: Colors.green,
            ),
            // Allows to add a border decoration around your table
            children: [
              TableRow(children: [
                Container(
                  color: Colors.greenAccent,
                  child: Text(
                    'วิชา:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  color: Colors.greenAccent,
                  child: Text(
                    'ห้องเรียน',
                    textAlign: TextAlign.center,
                  ),),
                Container(
                  color: Colors.greenAccent,
                  child: Text(
                    'เวลา',
                    textAlign: TextAlign.center,
                  ),),
              ]),


              TableRow(children: [
                Text(
                  '88634459-59',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'IF-4C02',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '10:00-12:00',
                  textAlign: TextAlign.center,
                ),
              ]),

              TableRow(children: [
                Text(
                  '88634259-59',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'IF-4C01',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '13:00-15:00',
                  textAlign: TextAlign.center,
                ),
              ]),

              TableRow(children: [
                Text(
                  '88624359-59',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'IF-3M210',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '15:00-17:00',
                  textAlign: TextAlign.center,
                ),
              ]),
            ]),
      ),
    );
  }
}


class tabel4 extends StatelessWidget {
  const tabel4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Center(
        child: Table(
            border: TableBorder.all(
              color: Colors.orange,
            ),
            // Allows to add a border decoration around your table
            children: [
              TableRow(children: [
                Container(
                  color: Colors.orangeAccent,
                  child: Text(
                    'วิชา:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  color: Colors.orangeAccent,
                  child: Text(
                    'ห้องเรียน',
                    textAlign: TextAlign.center,
                  ),),
                Container(
                  color: Colors.orangeAccent,
                  child: Text(
                    'เวลา',
                    textAlign: TextAlign.center,
                  ),),
              ]),


              TableRow(children: [
                Text(
                  '88646259-59',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'IF-5T05',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '9:00-12:00',
                  textAlign: TextAlign.center,
                ),
              ]),

              TableRow(children: [
                Text(
                  '88634459-59',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'IF-4C02',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '13:00-15:00',
                  textAlign: TextAlign.center,
                ),
              ]),

            ]),
      ),
    );
  }
}


class tabel5 extends StatelessWidget {
  const tabel5({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Center(
        child: Table(
            border: TableBorder.all(
              color: Colors.blue,
            ),
            // Allows to add a border decoration around your table
            children: [
              TableRow(children: [
                Container(
                  color: Colors.blueAccent,
                  child: Text(
                    'รหัสวิชา:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  color: Colors.blueAccent,
                  child: Text(
                    'ชื่อวิชา',
                    textAlign: TextAlign.center,
                  ),),
                Container(
                  color: Colors.blueAccent,
                  child: Text(
                    'กลุ่ม',
                    textAlign: TextAlign.center,
                  ),),
                Container(
                  color: Colors.blueAccent,
                  child: Text(
                    'กลางภาค',
                    textAlign: TextAlign.center,
                  ),),
                Container(
                  color: Colors.blueAccent,
                  child: Text(
                    'ปลายภาค',
                    textAlign: TextAlign.center,
                  ),),
              ]),


              TableRow(children: [
                Text(
                  '88624359-59',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Web Programming',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '2',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '16 ม.ค 2566 (17:00-20:00)  IF4C02',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '27 มี.ค 2566 (17:00-20:00)  IF4C02',
                  textAlign: TextAlign.center,
                ),
              ]),

              TableRow(children: [
                Text(
                  '88624459-59',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Object-Oriented Analysis and Design',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '2',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '17 ม.ค 2566 (17:00-20:00)  IF4C02',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '28 มี.ค 2566 (17:00-20:00)  IF4C02',
                  textAlign: TextAlign.center,
                ),
              ]),

              TableRow(children: [
                Text(
                  '88624559-59',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Software Testing',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '2',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '16 ม.ค 2566 (9:00-12:00)  IF4C02',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '27 มี.ค 2566 (9:00-12:00)  IF4C02',
                  textAlign: TextAlign.center,
                ),
              ]),

              TableRow(children: [
                Text(
                  '88634259-59',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Multimedia Programming for Multiplatforms',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '2',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '18 ม.ค 2566 (13:00-16:00)  IF4C02',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '29 มี.ค 2566 (13:00-16:00)  IF4C02',
                  textAlign: TextAlign.center,
                ),
              ]),

              TableRow(children: [
                Text(
                  '88634459-59',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Mobile Application Development I',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '2',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '20 ม.ค 2566 (13:00-16:00)  IF4C02',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '27 มี.ค 2566 (9:00-12:00)  IF4C02',
                  textAlign: TextAlign.center,
                ),
              ]),

              TableRow(children: [
                Text(
                  '88646259-59',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Introduction to Natural Language Processing',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '2',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '18 ม.ค 2566 (17:00-20:00)  IF4C02',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '31 มี.ค 2566 (9:00-12:00)  IF4C02',
                  textAlign: TextAlign.center,
                ),
              ]),

            ]),
      ),
    );
  }
}


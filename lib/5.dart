import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Five extends StatelessWidget {
  const Five({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: const Text('ผลการศึกษา'),
      ),
      body: ListView(
        children: [
          Text('\n'),
          Text('ผลการศึกษา',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 20.0,
              )),
          Image(
            image: NetworkImage(
                'https://scontent.fbkk20-1.fna.fbcdn.net/v/t1.6435-9/184401612_1505721702955404_5105613070502311784_n.jpg?_nc_cat=110&ccb=1-7&_nc_sid=174925&_nc_eui2=AeErTWM3aF6N8PPyTGlDHVM2lIIZS8SW27KUghlLxJbbshD8Ww3RNsgzp0snnm8RVDIwNARTGt5CuT_y0yl4_g4U&_nc_ohc=4UFTEWT0poIAX_Djdev&_nc_ht=scontent.fbkk20-1.fna&oh=00_AfCPksNwR-waT-3LFA--zqyB18JzWEXqMJthSuiHcZzhaw&oe=63F348EB'),
            width: 200,
            height: 300,
          ),
          Text('\n'),
          Text('ภาคการศึกษาที่ 1/2563',style: TextStyle(fontWeight: FontWeight.w600)),
          Text('\n'),
          tabel2(),
          Text('\n'),
          Text('ภาคการศึกษาที่ 2/2563',style: TextStyle(fontWeight: FontWeight.w600)),
          Text('\n'),
          tabel3(),
        ],
      ),
    );
  }
}

class tabel2 extends StatelessWidget {
  const tabel2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Center(
        child: Table(
            border: TableBorder.all(
              color: Colors.purple,
            ),
            // Allows to add a border decoration around your table
            children: [
              TableRow(children: [
                Container(
                  color: Colors.pinkAccent,
                  child: Text(
                    'รหัสวิชา',
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  color: Colors.pinkAccent,
                  child: Text(
                    'ชื่อรายวิชา',
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  color: Colors.pinkAccent,
                  child: Text(
                    'หน่วยกิต',
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  color: Colors.pinkAccent,
                  child: Text(
                    'เกรด',
                    textAlign: TextAlign.center,
                  ),
                ),
              ]),
              TableRow(children: [
                Text(
                  '30910159',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Marine Ecology and Ecotourism',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '2',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'A',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Text(
                  '40240359',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Sufficiency Economy and Social Development',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '2',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'A',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Text(
                  '85111059',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Exercise for Quality of Life',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '2',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'A',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Text(
                  '88510059',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Logical Thinking and Problem Solving for Innovation',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '2',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'A',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Text(
                  '88510159',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Moving Forward in a Digital Society with ICT',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '3',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'A',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Text(
                  '88510259',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Discrete Structures',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '3',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'C',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Text(
                  '99910259',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Collegiate English',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '3',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'A',
                  textAlign: TextAlign.center,
                ),
              ]),
            ]),
      ),
    );
  }
}

class tabel3 extends StatelessWidget {
  const tabel3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Center(
        child: Table(
            border: TableBorder.all(
              color: Colors.blue,
            ),
            // Allows to add a border decoration around your table
            children: [
              TableRow(children: [
                Container(
                  color: Colors.blueGrey,
                  child: Text(
                    'รหัสวิชา',
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  color: Colors.blueGrey,
                  child: Text(
                    'ชื่อรายวิชา',
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  color: Colors.blueGrey,
                  child: Text(
                    'หน่วยกิต',
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  color: Colors.blueGrey,
                  child: Text(
                    'เกรด',
                    textAlign: TextAlign.center,
                  ),
                ),
              ]),
              TableRow(children: [

                Text(
                  '61010159',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Art and Life',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '3',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'B',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Text(
                  '77037959',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Arts and Creativity',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '2',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'A',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Text(
                  '88510359',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Mathematics for Computing',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '3',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'C',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Text(
                  '88510459',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Programming Fundamental',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '3',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'F',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Text(
                  '88612159',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Introduction to Computer Science',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '3',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'C+',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Text(
                  '99920159',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'English Writing for Communication',
                  textAlign: TextAlign.center,
                ),
                Text(
                  '3',
                  textAlign: TextAlign.center,
                ),
                Text(
                  'C+',
                  textAlign: TextAlign.center,
                ),
              ]),
            ]),
      ),
    );
  }
}

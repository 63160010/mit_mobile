import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Four extends StatelessWidget {
  const Four({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: const Text('ประวัตินิสิต'),
      ),
      body: ListView(
        children: [
          Text('\n'),
          Text('ประวัตินิสิต',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,

                fontSize: 20.0,
              )),
          Image(
            image: NetworkImage(
                'https://scontent.fbkk20-1.fna.fbcdn.net/v/t1.6435-9/184401612_1505721702955404_5105613070502311784_n.jpg?_nc_cat=110&ccb=1-7&_nc_sid=174925&_nc_eui2=AeErTWM3aF6N8PPyTGlDHVM2lIIZS8SW27KUghlLxJbbshD8Ww3RNsgzp0snnm8RVDIwNARTGt5CuT_y0yl4_g4U&_nc_ohc=4UFTEWT0poIAX_Djdev&_nc_ht=scontent.fbkk20-1.fna&oh=00_AfCPksNwR-waT-3LFA--zqyB18JzWEXqMJthSuiHcZzhaw&oe=63F348EB'),
            width: 200,
            height: 300,
          ),
          Text('\n'),
          Divider(
            height: 10,
          ),
          Text('\n'),
          Text(
            'ข้อมูลด้านการศึกษา',
            textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.w600)
          ),
          Text('\n'),
          tabel2(),
          Text('\n'),
          Text(
            'ข้อมูลส่วนบุคคล',
            textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.w600)
          ),
          Text('\n'),
          tabel3(),
        ],
      ),
    );
  }
}

class tabel2 extends StatelessWidget {
  const tabel2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Center(
        child: Table(
            border: TableBorder.all(
              color: Colors.blue.shade900,
            ),
            // Allows to add a border decoration around your table
            children: [
              TableRow(children: [
                Container(
                  color: Colors.blue,
                  height: 20,
                  child: Text(
                    'รหัสประจำตัว:',
                    textAlign: TextAlign.center,

                  ),
                ),
                Text(
                  '63160010 ',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.blue,
                  height: 20,
                  child: Text(
                    'เลขที่บัตรประชาชน:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  'XXXX400080XXX ',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.blue,
                  height: 20,
                  child: Text(
                    'ชื่อ:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  'นางสาวมนัสชนก แซ่เตียว ',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.blue,
                  height: 40,
                  child: Text(
                    'ชื่ออังกฤษ:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  'MISS MANASCHANOK SAETIAW ',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.blue,
                  height: 20,
                  child: Text(
                    'คณะ:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  'คณะวิทยาการสารสนเทศ',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.blue,
                  height: 20,
                  child: Text(
                    'วิทยาเขต:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  'บางแสน',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.blue,
                  height: 80,
                  child: Text(
                    'หลักสูตร:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  '2115020 วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.blue,
                  height: 20,
                  child: Text(
                    'วิชาโท:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  '-',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.blue,
                  height: 20,
                  child: Text(
                    'ระดับการศึกษา:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  'ปริญญาตรี  ',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.blue,
                  height: 80,
                  child: Text(
                    'ชื่อปริญญา:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  'วิทยาศาสตรบัณฑิต วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ ',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.blue,
                  height: 20,
                  child: Text(
                    'ปีการศึกษาที่เข้า:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  '2563 / 1    วันที่ 5/2/2563 ',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.blue,
                  height: 20,
                  child: Text(
                    'สถานภาพ::',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  '-',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.blue,
                  height: 60,
                  child: Text(
                    'วิธีรับเข้า:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  'โครงการความร่วมมือทางวิชาการ (MOU)',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.blue,
                  height: 20,
                  child: Text(
                    'วุฒิก่อนเข้ารับการศึกษา:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  'ม.6   3.47 ',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.blue,
                  height: 20,
                  child: Text(
                    'จบการศึกษาจาก:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  'ชำฆ้อพิทยาคม ',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.blue,
                  height: 60,
                  child: Text(
                    'อ. ที่ปรึกษา:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  'ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม ',
                  textAlign: TextAlign.center,
                ),
              ]),
            ]),
      ),
    );
  }
}

class tabel3 extends StatelessWidget {
  const tabel3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Center(
        child: Table(
            border: TableBorder.all(
              color: Colors.blue,
            ),
            // Allows to add a border decoration around your table
            children: [
              TableRow(children: [
                Container(
                  color: Colors.yellow,
                  height: 20,
                  child: Text(
                    'สัญชาติ:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  'ไทย',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.yellow,
                  height: 20,
                  child: Text(
                    'ศาสนา:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  'พุทธ',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.yellow,
                  height: 20,
                  child: Text(
                    'หมู่เลือด:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  'O',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.yellow,
                  height: 50,
                  child: Text(
                    'ที่อยู่',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  'คลองพลู เขต/อำเภอ หนองใหญ่  ชลบุรี 20190  โทร: 0868509307',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.yellow,
                  height: 20,
                  child: Text(
                    'สถานะการทำงาน:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  '-',
                  textAlign: TextAlign.center,
                ),
              ]),
              TableRow(children: [
                Container(
                  color: Colors.yellow,
                  height: 20,
                  child: Text(
                    'ตำแหน่ง:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  '-',
                  textAlign: TextAlign.center,
                ),
              ]),
            ]),
      ),
    );
  }
}

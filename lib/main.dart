import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:reg/1.dart';
import 'package:reg/2.dart';
import 'package:reg/3.dart';
import 'package:reg/4.dart';
import 'package:reg/5.dart';
import 'package:reg/6.dart';
import 'package:reg/7.dart';


void main() => runApp(const MaterialApp(
  title: 'Navigation Basics',
  home: MyApp(),
));

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'REG',
        theme: ThemeData(
          primarySwatch: Colors.grey,
        ),
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: Text('เมนูหลัก'),
            backgroundColor: Colors.grey,
          ),
          body:Container(
            padding: EdgeInsets.all(35),
            child: ListView(
              // crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: Image(image: NetworkImage('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS677JiDZqiJD9wqTzItjZdD6GpA10suIId_tvBkZTadIRywud0EyqNgnLKufQ-vBvtDrQ&usqp=CAU')
            ,fit: BoxFit.cover,),
                ),
                Text('\n \n'),
                Container(child: Text(
                  '63160010 : นางสาวมนัสชนก แซ่เตียว : คณะวิทยาการสารสนเทศ  หลักสูตร: 2115020: วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี \n สถานภาพ: กำลังศึกษา  อ. ที่ปรึกษา: ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม \n \n',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: 15.0,
                  ),
                ),
                  ),


                Divider(
                  height: 10,
                ),
                Center(
                  child: Text(
                    'ประกาศ',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.red,
                      fontWeight: FontWeight.w600,
                      fontSize: 20.0,
                    ),
                  ),
                ),

                Divider(
                  height: 10,
                ),
                Container(
                  child: Image(image: NetworkImage('https://scontent.fbkk20-1.fna.fbcdn.net/v/t1.6435-9/138564935_3680627968682961_4505382183359906840_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=973b4a&_nc_ohc=84Nc7Emam_QAX8zhGzX&_nc_ht=scontent.fbkk20-1.fna&oh=00_AfAy6YMrTp7eQ0blqQS0lfjuvLH5SvxdkExu-rdGcmXDOA&oe=63F39956')
                    ,fit: BoxFit.cover,),

                ),
                Text('\n '),
                Container(
                  child: Image(image: NetworkImage('https://pbs.twimg.com/media/E2UaV9HUYAMFNVl?format=jpg&name=4096x4096',), fit: BoxFit.cover),

                ),
                Text('\n '),
                Container(
                  child: Image(image: NetworkImage('https://pbs.twimg.com/media/FG0EHlyVcAgzFxT?format=jpg&name=900x900')
                    ,fit: BoxFit.cover,),
                ),


              ],
            ),

          ),

          ///////////
          drawer: DrawerCodeOnly(),

        ),
      ),
    );
  }
}



class DrawerCodeOnly extends StatelessWidget {
  const DrawerCodeOnly({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Drawer(
      elevation: 16.0,
      child: new ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text('Manutcharnok Saetiaw',style:TextStyle(
              color: Colors.white,),),
            accountEmail: Text('63160010@go.buu.ac.th',style:TextStyle(
              color: Colors.white,)),
            currentAccountPicture: CircleAvatar(
              backgroundImage: NetworkImage(
                  'https://scontent.fbkk20-1.fna.fbcdn.net/v/t1.6435-9/184401612_1505721702955404_5105613070502311784_n.jpg?_nc_cat=110&ccb=1-7&_nc_sid=174925&_nc_eui2=AeErTWM3aF6N8PPyTGlDHVM2lIIZS8SW27KUghlLxJbbshD8Ww3RNsgzp0snnm8RVDIwNARTGt5CuT_y0yl4_g4U&_nc_ohc=4UFTEWT0poIAX_Djdev&_nc_ht=scontent.fbkk20-1.fna&oh=00_AfCPksNwR-waT-3LFA--zqyB18JzWEXqMJthSuiHcZzhaw&oe=63F348EB'),
            ),
            otherAccountsPictures: <Widget>[
              CircleAvatar(
                backgroundImage: NetworkImage(
                    'https://upload.wikimedia.org/wikipedia/commons/e/ec/Buu-logo11.png'),
              )
            ],
          ),
          new ListTile(
            leading: const Icon(Icons.laptop_chromebook),
            title: const Text('ลงทะเบียน'),
            onTap: () {

              Navigator.push(context, new MaterialPageRoute(builder: (context) => new One()));
            },
          ),
          Divider(
            height: 0.1,
          ),
          new ListTile(
            leading: const Icon(Icons.assignment_turned_in),
            title: const Text('ผลการลงทะเบียน'),
            onTap: () {
              Navigator.push(context, new MaterialPageRoute(builder: (context) => new Two()));
            },
          ),
          Divider(
            height: 0.1,
          ),
          new ListTile(
            leading: const Icon(Icons.assignment_returned_sharp),
            title: const Text('ผลอนุมัติเพิ่ม-ลด'),
            // onTap: () {Navigator.push(context, new MaterialPageRoute(builder: (context) => new Five()));
            // },
          ),
          Divider(
            height: 0.1,
          ),
          new ListTile(
            leading: const Icon(Icons.card_travel),
            title: const Text('ตารางเรียน / สอบ'),
            onTap: () {Navigator.push(context, new MaterialPageRoute(builder: (context) => new Three()));
            },
          ),
          Divider(
            height: 0.1,
          ),
          new ListTile(
            leading: const Icon(Icons.folder_shared),
            title: const Text('ประวัตินิสิต'),
            onTap: () {Navigator.push(context, new MaterialPageRoute(builder: (context) => new Four()));
            },
          ),
          Divider(
            height: 0.1,
          ),
          new ListTile(
            leading: const Icon(Icons.monetization_on_outlined),
            title: const Text('ภาระค่าใช้จ่ายทุน'),
            // onTap: () {Navigator.push(context, new MaterialPageRoute(builder: (context) => new Five()));
            // },
          ),
          Divider(
            height: 0.1,
          ),
          new ListTile(
            leading: const Icon(Icons.school),
            title: const Text('ผลการศึกษา'),
            onTap: () {Navigator.push(context, new MaterialPageRoute(builder: (context) => new Five()));
            },
          ),
          Divider(
            height: 0.1,
          ),
          new ListTile(
            leading: const Icon(Icons.school_outlined),
            title: const Text('ตรวจสอบจบ'),
            // onTap: () {Navigator.push(context, new MaterialPageRoute(builder: (context) => new Five()));
            // },
          ),
          Divider(
            height: 0.1,
          ),
          new ListTile(
            leading: const Icon(Icons.inbox),
            title: const Text('ยื่นคำร้อง'),
            // onTap: () {Navigator.push(context, new MaterialPageRoute(builder: (context) => new Five()));
            // },
          ),
          Divider(
            height: 0.1,
          ),
          new ListTile(
            leading: const Icon(Icons.mail),
            title: const Text('เสนอความคิด'),
            onTap: () {Navigator.push(context, new MaterialPageRoute(builder: (context) => new Six()));
            },
          ),
          Divider(
            height: 0.1,
          ),

          new ListTile(
            leading: const Icon(Icons.people),
            title: const Text('รายชื่อนิสิต'),
            // onTap: () {Navigator.push(context, new MaterialPageRoute(builder: (context) => new Five()));
            // },
          ),
          Divider(
            height: 0.1,
          ),
          new ListTile(
            leading: const Icon(Icons.history),
            title: const Text('ประวัติการเข้าใช้ระบบ'),
            onTap: () {Navigator.push(context, new MaterialPageRoute(builder: (context) => new Seven()));
            },
          ),
          Divider(
            height: 0.1,
          ),
          new ListTile(
            leading: const Icon(Icons.credit_card),
            title: const Text('ตรวจสอบหนี้สิน(นิสิตที่ยังไม่จบ)',style: TextStyle(color: Colors.red),),
            // onTap: () {Navigator.push(context, new MaterialPageRoute(builder: (context) => new Five()));
            // },
          ),
          Divider(
            height: 0.1,
          ),
        ],
      ),

    );
  }
}
